<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Illuminate\Contracts\Auth\Guard;
use Auth;
class AdministratorUserAccount
{
    protected $auth;
    
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if (!$this->auth->guest()) {
            if($this->auth->user()->id_rol == 2 && $this->auth->user()->status = 1) {
               return $next($request);   
            } else {
                abort(401, 'Unauthorized action');       
            }
        } else {
            abort(401, 'Unauthorized action');   
        }
    }
}
