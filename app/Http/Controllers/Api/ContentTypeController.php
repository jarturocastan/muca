<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\ElementType;
use App\Models\ContentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContentTypeElementType;

class ContentTypeController extends Controller
{
    public function storage($id_element_type,Request $request) 
    {
        $user = Auth::user();
        $elementType = ElementType::find($id_element_type);
        $contentType = new ContentType;
        $contentType->type = $request->type;
        $contentType->attribute = $request->attribute;
        $contentType->name = $request->name;
        $contentType->status = $request->status;
        $contentType->created_user = $user->id_user;
        $contentType->updated_user = $user->id_user;
        $contentType->save();
        $contentTypeElementType = new ContentTypeElementType; 
        $contentTypeElementType->elementType()->associate($elementType);
        $contentTypeElementType->contentType()->associate($contentType);
        $contentTypeElementType->created_user = $user->id_user;
        $contentTypeElementType->updated_user = $user->id_user;
        $contentTypeElementType->save();
        return response()->json($contentType);
    }
}
