<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\ElementType;
use App\Models\ContentType;
use App\Models\ContentTypeValidator;
use App\Models\ContentTypeElementType;
use App\Models\ValidatorField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ElementTypeController extends Controller
{
    public function storage(Request $request) 
    {
        $user = Auth::user();
        $elementType = new ElementType;
        $elementType->name = $request->name;
        $elementType->status = $request->status;
        $elementType->created_user = $user->id_user;
        $elementType->updated_user = $user->id_user;
        $elementType->save();
        return response()->json($elementType);
    }

    public function findAllActives() 
    {
        return response()->json(ElementType::findAllActives());   
    }

    public function findAllInactives() 
    {
        return response()->json(ElementType::findAllInactives());   
    }

    public function getBlockWords($id_element_type) 
    {
        $user = Auth::user();
        $elementType = ElementType::find($id_element_type);
        $contentTypeElementType = ContentTypeElementType::findByElmentTypeIdAndContentTypeId($id_element_type,5);
        if($contentTypeElementType != null) {
            $validator = ContentTypeValidator::findAllValidationByContentTypeIdAndValidatorName(5,'block_sound');
            return response()->json($validator->rules);
        }

        return response()->json($contentTypeElementType);
    } 


    public function saveBlockWord($id_element_type, Request $request) 
    {
        $user = Auth::user();
        $elementType = ElementType::find($id_element_type);
        $contentType = ContentType::where('name', strtolower($elementType->name))->first();
        $contentTypeValidator = ContentTypeValidator::where('id_content_type',$contentType->id_content_type)->first();
        $validatorField = new ValidatorField;
        $validatorField->rule = $request->input("word");
        $validatorField->contentTypeValidator()->associate($contentTypeValidator);
        $validatorField->created_user = $user->id_user;
        $validatorField->updated_user = $user->id_user;
        $validatorField->save();
        return response()->json($validatorField);
    }
}
