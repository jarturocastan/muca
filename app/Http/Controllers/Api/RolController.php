<?php

namespace App\Http\Controllers\Api;

use App\Models\Rol;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class RolController extends Controller
{

    public function findAllActives()  
    {
        return response()->json(Rol::actives()->get());
    }
}
