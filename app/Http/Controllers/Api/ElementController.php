<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\Element;
use App\Models\Content;
use App\Models\ElementType;
use App\Models\ContentTypeElementType;
use App\Models\ContentTypeValidator;
use Illuminate\Http\Request;
use Illuminate\Http\ContentType;
use App\Http\Controllers\Controller;
use Zipper;
use Google\Cloud\Speech\SpeechClient;



class ElementController extends Controller
{

    public function storage($id_element_type,Request $request) 
    {
        $user = Auth::user();
        $elementType = ElementType::find($id_element_type);
        $contentTypesElementTypes = $elementType->contentTypesElementTypes;
        $element = null;
        foreach ($contentTypesElementTypes as $key => $contentTypeElementType) {
            $contentType = $contentTypeElementType->contentType;
            
            if ($request->hasFile($contentType->name)) { 
                $file = $request->file($contentType->name);
                $fileName = str_random(10).$file->getClientOriginalName();
                if($element == null) {
                    $element = new Element;
                }
                $element->name = $fileName;
                $element->elementType()->associate($elementType);
                $element->created_user = $user->id_user;
                $element->updated_user = $user->id_user;
                $element->save();
                $path = 'public/'.$elementType->id_element_type.'/'.$element->id_element;
                $pathElement = $file->storeAs($path,$fileName);
                $content = new Content;
                $content->content = $pathElement;
                $content->element()->associate($element);
                $content->contentTypeElementType()->associate($contentTypeElementType);
                $content->created_user = $user->id_user;
                $content->updated_user = $user->id_user;
                $content->save();
                $projectId = 'pure-mission-210403';
                $fileName = storage_path('app')."/".$pathElement;
                //$this->streaming_recognize($fileName,'es-MX','LINEAR16',16000);
                //exit();
               /* $speech = new SpeechClient([
                    'keyFilePath' => storage_path('app').'/Google.json',
                    'projectId' => $projectId,
                    'languageCode' => 'es-MX',
                ]);
                $options = [
                    'encoding' => 'LINEAR16',
                    
                ];
                $results = $speech->recognize(fopen($fileName, 'r'));
                $textSpeech = "";
                foreach ($results as $result) {
                    $textSpeech = $textSpeech." ".$result->alternatives()[0]['transcript'];
                }

                $validator = ContentTypeValidator::findAllValidationByContentTypeIdAndValidatorName($contentTypeElementType->id_content_type,'block_sound');
                if($validator != null) {
                    $isInvalid = 0;
                    foreach ($validator->rules as $key => $rule) {
                        if(strpos($textSpeech, $rule->rule)) {
                            $isInvalid = 1;
                        }
                    }
                    
                    $contentTypeElementType = ContentTypeElementType::find(4);
                    $content = new Content;
                    $content->content ='{"attention":'.$isInvalid.'}';
                    $content->element()->associate($element);
                    $content->contentTypeElementType()->associate($contentTypeElementType);
                    $content->created_user = $user->id_user;
                    $content->updated_user = $user->id_user;
                    $content->save();               
                }   */
            } else {
                $input = $request->input($contentType->name);
                if($input != null) {
                    $validAttribute = true;
                    if($contentType->attribute == "boolean") {
                        if(!is_bool($input)) {
                            $validAttribute = false;
                        } else {
                            $input = boolval($input); 
                        }
                    }
    
                    if($validAttribute == false) {
                        return response()->json("Atributo invalido",401);
                    }
    
                    if($element == null) {
                        $element = new Element;
                        $element->name = str_random(10);
                        $element->elementType()->associate($elementType);
                        $element->created_user = $user->id_user;
                        $element->updated_user = $user->id_user;
                        $element->save();
                    }
                    $content = new Content;
                    $content->content ='{"'.$contentType->name.'":'.$input.'}';
                    $content->element()->associate($element);
                    $content->contentTypeElementType()->associate($contentTypeElementType);
                    $content->created_user = $user->id_user;
                    $content->updated_user = $user->id_user;
                    $content->save();
                }
            }
        }

        return response()->json("Audio guardado exitosamente",200);        
    }

    function streaming_recognize($audioFile, $languageCode, $encoding, $sampleRateHertz)
    {   
        if (!defined('Grpc\STATUS_OK')) {
            throw new \Exception('Install the grpc extension ' .
                '(pecl install grpc)');
        }
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.storage_path('app').'/Google.json');
        $speechClient = new SpeechClient([
            'keyFilePath' => storage_path('app').'/Google.json',
            'projectId' => 'pure-mission-210403',
            'languageCode' => 'es-MX',
        ]);
        try {
            $config = new RecognitionConfig();
            $config->setLanguageCode($languageCode);
            $config->setSampleRateHertz($sampleRateHertz);
            // encoding must be an enum, convert from string
            $encodingEnum = constant(RecognitionConfig_AudioEncoding::class . '::' . $encoding);
            $config->setEncoding($encodingEnum);

            $strmConfig = new StreamingRecognitionConfig();
            $strmConfig->setConfig($config);

            $strmReq = new StreamingRecognizeRequest();
            $strmReq->setStreamingConfig($strmConfig);

            $strm = $speechClient->streamingRecognize();
            $strm->write($strmReq);

            $strmReq = new StreamingRecognizeRequest();
            $f = fopen($audioFile, "rb");
            $fsize = filesize($audioFile);
            $bytes = fread($f, $fsize);
            $strmReq->setAudioContent($bytes);
            $strm->write($strmReq);

            foreach ($strm->closeWriteAndReadAll() as $response) {
                foreach ($response->getResults() as $result) {
                    foreach ($result->getAlternatives() as $alt) {
                        printf("Transcription: %s\n", $alt->getTranscript());
                    }
                }
            }
        } finally {
            $speechClient->close();
        }
    }

    public function element($id_element_type) 
    { 
        $session = \Session::get('favoriteCount'); 
        if(empty($session)) {
            \Session::put('favoriteCount', ["favorite_count" => 1 ]); 
            $session = \Session::get('favoriteCount'); 
            $favorite_count =  $session["favorite_count"];
        } else {
            $favorite_count =  $session["favorite_count"];
        }   
        $favorite = ($favorite_count <= 3) ? 1 : 0;
        $contentFavorite = null;
        $id_element = null;
        $contentFavorite = Content::getRandomFavoriteContentByContentTypeIdAndElementTypeId($id_element_type,7,$favorite);
        
        if($contentFavorite == null) {
            if($favorite == 1) {
                $element = Element::getRandomElementByElementTypeId(1);
                if($element == null) {
                    \Session::put('favoriteCount', ["favorite_count" => ($favorite_count + 1) ]); 
                    return response()->json("No hay audios en el sistema",402);
                } else {
                    $id_element = $element->id_element;
                }
            } else {
                $contentFavorite = Content::getRandomFavoriteContentByContentTypeIdAndElementTypeId($id_element_type,7,1);
                if($contentFavorite == null) {
                    \Session::put('favoriteCount', ["favorite_count" => ($favorite_count + 1) ]); 
                    return response()->json("No hay audios en el sistema",402);
                } else {
                    $id_element = $contentFavorite->id_element;
                }
            }
        } else {
            $id_element = $contentFavorite->id_element;
        }

        if($favorite == 1) {
            \Session::put('favoriteCount', ["favorite_count" => ($favorite_count + 1) ]); 
        } else {
            \Session::put('favoriteCount', ["favorite_count" => 1 ]); 
        }


        $element = Element::findElementWithContentsByElementId($id_element);
        $content = $element->contents[strtolower($element->elementType->name)];
        return \Storage::download($content["content"], basename($content["content"]));
    } 

    public function file($id_element) 
    {
        $element = Element::findElementWithContentsByElementId($id_element);
        $content = $element->contents[strtolower($element->elementType->name)];
        return \Storage::download($content["content"]);
    }

    public function findAllActives() 
    {
        return response()->json(Element::findAllActives());   
    }

    public function findAllToday() 
    {
        return response()->json(Element::findAllToday());   
    }

    public function findAllFavoriteToday() 
    {
        return response()->json(Element::findAllTodayByContentAndContentTypeIdElemntTypeId(3,1)); 
    }

    public function findAllAttentionToday() 
    {
        return response()->json(Element::findAllTodayByContentAndContentTypeIdElemntTypeId(4,1)); 
    }

    public function findAllTodayActives() 
    {
        return response()->json(Element::findAllTodayActives());   
    }

    public function findAllByDate(Request $request) 
    {
       $date = $request->input('date');
       return response()->json(Element::findAllByDate($date));
    }

    public function findAllFavoriteByDate(Request $request) 
    {
        $date = $request->input('date');
        return response()->json(Element::findAllByContentAndContentTypeIdElemntTypeIdByDate(3,1,$date)); 
    }

    public function findAllAttentionByDate(Request $request) 
    {
        $date = $request->input('date');
        return response()->json(Element::findAllByContentAndContentTypeIdElemntTypeIdByDate(4,1,$date)); 
    }

    public function addFavorite($id_element) 
    {
        $user = Auth::user();
        $element = Element::find($id_element);
        $id_content_type_element_type = 3;
        $contentTypeElementType = ContentTypeElementType::find($id_content_type_element_type);
        $content = Content::getPathContentByElementIdAndContentTypeElementType($id_element,$id_content_type_element_type);
        if($content == null) {
            $content = new Content;
            $content->content ='{"'.$contentTypeElementType->contentType->name.'": 1}';
            $content->element()->associate($element);
            $content->contentTypeElementType()->associate($contentTypeElementType);
            $content->created_user = $user->id_user;
            $content->updated_user = $user->id_user;
            $content->save();
            $element = Element::findElementWithContentsByElementId($id_element);
            return response()->json($element);
        } else {
            $contentJson = json_decode($content->content);
            $favorite = $contentJson->{$contentTypeElementType->contentType->name};
            if($favorite == 1) {
                $content->content ='{"'.$contentTypeElementType->contentType->name.'": 0}';
            } else {
                $content->content ='{"'.$contentTypeElementType->contentType->name.'": 1}';
            }
            $content->save();
            $element = Element::findElementWithContentsByElementId($id_element);
            return response()->json($element);
        }
    }

    public function addAttention($id_element,$attention) 
    {
        $user = Auth::user();
        $element = Element::find($id_element);
        $id_content_type_element_type = 4;
        $contentTypeElementType = ContentTypeElementType::find($id_content_type_element_type);
        $content = Content::getPathContentByElementIdAndContentTypeElementType($id_element,$id_content_type_element_type);
        if($content == null) {
            $content = new Content;
            $content->content ='{"'.$contentTypeElementType->contentType->name.'": '.$attention.'}';
            $content->element()->associate($element);
            $content->contentTypeElementType()->associate($contentTypeElementType);
            $content->created_user = $user->id_user;
            $content->updated_user = $user->id_user;
            $content->save();
            $element = Element::findElementWithContentsByElementId($id_element);
            return response()->json($element);
        } else {
            $content->content ='{"'.$contentTypeElementType->contentType->name.'": '.$attention.'}';
            $content->updated_user = $user->id_user;
            $content->save();
            $element = Element::findElementWithContentsByElementId($id_element);
            return response()->json($element);
        }
    }

    public function delete($id_element) 
    {
        $element = Element::find($id_element);
        if($element != null) {
            $element->delete();
        }
        return response()->json($element);
    }

    public function modifyStatus($id_element) 
    {
        $element = Element::find($id_element);
        if($element->status == 1) {
            $element->status = 0;
        } else {
            $element->status = 1;
        }
        $element->save();
        $element = Element::findElementWithContentsByElementId($id_element);
        return response()->json($element);
    }

    public function deleteMany(Request $request) 
    {
        $elementIds = $request->input('elementIds');
        $elements = Element::findMany($elementIds);
        foreach ($elements as $key => $element) {
            $element->delete();
        }

        return response()->json($elementIds);
    }

    public function creteZip(Request $request) 
    {
        if(\Storage::exists('public/audios.zip')) {
            \Storage::delete('public/audios.zip');
            \Storage::deleteDirectory("public/audios_zip/");
        }
        $path = 'storage/audios.zip';
        $files = array();
        $elementIds = $request->input('elementIds');
        foreach ($elementIds as $key => $id_element) {
            $element = Element::findElementWithContentsByElementId($id_element);
            $content = $element->contents[strtolower($element->elementType->name)];
            \Storage::copy($content["content"], 'public/audios_zip/'.$element->name);
        }

        Zipper::make($path)->add(glob("storage/audios_zip"))->close();
        \Storage::deleteDirectory("public/audios_zip/");
        return response()->json($elementIds);;
    }

    public function getZip() 
    {
        return \Storage::download('public/audios.zip');
    }

    public function getCalendarInfo(Request $request) 
    {
        $calendar = array();
        $date = $request->input('date');  
        $countDays = date('t', strtotime($date));
        $date = $request->input('date')."-";  
        $week = $this->numberOfWeek($countDays,date('d', strtotime($date)),date('0', strtotime($date)));
        for ($i=1; $i <= $countDays; $i++) { 
            $date = $date.$i;
            $day_of_week = date('l', strtotime($date));
            $favorite_count = 6;
            //$count_elements_attention = Element::countAllTodayByContentAndContentTypeIdElemntTypeIdAndDate(4,$date);
            $all_count = Element::createdAt($date)->count();
            $date = $request->input('date')."-";  
            array_push($calendar, array(
                "number" => $i,
                "name" => $day_of_week,
                "week" => $week,
                "favorite" => $favorite_count,
                "attention" => 0,
                "allowed" =>0,
                "all" => $all_count,
                "countdays" => $countDays,
                "date" =>  $date
            ));
        }
        return response()->json($calendar);
    }

    function numberOfWeek ($dia, $mes, $ano) {
        $fecha = mktime (23, 59, 59, $mes, $dia, $ano);
        $numberOfWeek = ceil (($dia + (date ("w", $fecha)-1)) / 7);
        return $numberOfWeek;
    }

}
