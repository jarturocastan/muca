<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\User;
use App\Models\Rol;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function findAllActives() 
    {
        return response()->json(User::findAllActives());
    }

    public function findAllInactives() 
    {
        return response()->json(User::findAllInactives());
    }

    public function modifyStatus($id_user) 
    {
        $user = User::find($id_user);
        if($user->status == 1) {
            $user->status = 0;
        } else {
            $user->status = 1;
        }
        $user->save();
        return response()->json($user);
    }

    public function delete($id_element) 
    {
        $user = User::find($id_element);
        if($user != null) {
            $user->delete();
        }
        return response()->json($user);
    }

    public function storage(Request $request) 
    {
        $user = Auth::user();
        $id_rol = $request->input('id_rol');
        $rol = Rol::find($id_rol);
        if($rol != null) {
            $email = $request->input('email');
            $password = $request->input('password');
            $password_confirm = $request->input('password_confirm');
            if($password == $password_confirm) {
             
                $userNew = new User; 
                $userNew->email = $email;
                $userNew->password = bcrypt($password);
                $userNew->status = $request->input('status');
                $userNew->rol()->associate($rol);
                $userNew->created_user = $user->id_user;
                $userNew->updated_user = $user->id_user;
                $userNew->save();
                return response()->json($user);
            }
        }
    }

    public function deleteMany(Request $request) 
    {
        $userIds = $request->input('userIds');
        $users = User::findMany($userIds);
        foreach ($users as $key => $user) {
            $user->delete();
        }
        return response()->json($userIds);
    }
}
