<?php

namespace App\Models;

use App\Models\ValidatorField;
use Illuminate\Database\Eloquent\Model;

class ContentTypeValidator extends Model
{
    protected $table = 'content_type_validator';
    protected $primaryKey = 'id_content_type_validator';

    public function validator()
    {
        return $this->belongsTo('App\Models\Validator','id_validator');
    }

    public function contentType()
    {
        return $this->belongsTo('App\Models\ContentType','id_content_type');
    }

    public function validatorFields()
    {
        return $this->hasMany('App\Models\ValidatorField','id_validator_field');
    }

    public static function findAllValidationByContentTypeIdAndValidatorName($id_content_type,$validator_name) 
    {
        $validator = null;
        foreach (self::where('id_content_type',$id_content_type)->with('validator')->cursor() as $key => $contentTypeValidator) {
            if($contentTypeValidator->validator->name == $validator_name) {
                $validator = $contentTypeValidator->validator;
                $validator->rules = ValidatorField::select('rule')->where('id_content_type_validator',$contentTypeValidator->id_content_type_validator)->get();
            }
        }
        return $validator;
    }
}
