<?php

namespace App\Models;
use  App\Models\Content;
use  App\Models\ContentTypeElementType;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $table = 'elements';
    protected $primaryKey = 'id_element';

    public function elementType()
    {
        return $this->belongsTo('App\Models\ElementType','id_element_type');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Content','id_content');
    }

    public function scopeElementTypeId($query,$id_element_type) 
    {
        return $query->where('id_element_type',$id_element_type);
    }

    public function scopeCreatedAt($query,$date) 
    {
        return $query->whereDate('created_at', '=', $date);
    }

    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    /**Function */
    public static function findAllActives() 
    {
        return self::actives()->get();
    }

    public static function findElementWithContentsByElementId($id_element) 
    {
        $element = self::find($id_element);
        if($element != null) {
            $contents =  Content::actives()->elementId($id_element)->get();
            $element->contents = self::contentsFormatt($contents);
        }
        return $element;
    }

    public static function findAllTodayActives() 
    {
        $elements = [];
        foreach (self::actives()->createdAt(date('Y-m-d'))->cursor() as $key => $element) {
           $contents =  Content::actives()->elementId($element->id_element)->get();
           $element->contents = self::contentsFormatt($contents);
            
           array_push($elements,$element);
        }
        return $elements;
    }

    public static function findAllToday() 
    {
        $elements = [];
        foreach (self::createdAt(date('Y-m-d'))->cursor() as $key => $element) {
           $contents =  Content::actives()->elementId($element->id_element)->get();
           $element->contents = self::contentsFormatt($contents);
            
           array_push($elements,$element);
        }
        return $elements;
    }

    public static function findAllByDate($date) 
    {
        $elements = [];
        foreach (self::createdAt($date)->cursor() as $key => $element) {
           $contents =  Content::actives()->elementId($element->id_element)->get();
           $element->contents = self::contentsFormatt($contents);
            
           array_push($elements,$element);
        }
        return $elements;
    }

    public static function findAllTodayByContentAndContentTypeIdElemntTypeId($id_content_type_element_type,$value) 
    {
        $elements = [];
        $contentTypeElementType = ContentTypeElementType::find($id_content_type_element_type);
        foreach (self::createdAt(date('Y-m-d'))->cursor() as $key => $element) {
            $contents =  Content::actives()->elementId($element->id_element)->get();
            $element->contents = self::contentsFormatt($contents);
            if($contentTypeElementType != null) {
                if(isset($element->contents[$contentTypeElementType->contentType->name])) {
                    $content = $element->contents[$contentTypeElementType->contentType->name];
                    if($content["content"] == $value) {
                        array_push($elements,$element); 
                    }
                } else if($value == 0) {
                    array_push($elements,$element);     
                }
            }
        }
        return $elements;
    }

    public static function findAllByContentAndContentTypeIdElemntTypeIdByDate($id_content_type_element_type,$value,$date) 
    {
        $elements = [];
        $contentTypeElementType = ContentTypeElementType::find($id_content_type_element_type);
        foreach (self::createdAt($date)->cursor() as $key => $element) {
            $contents =  Content::actives()->elementId($element->id_element)->get();
            $element->contents = self::contentsFormatt($contents);
            if($contentTypeElementType != null) {
                if(isset($element->contents[$contentTypeElementType->contentType->name])) {
                    $content = $element->contents[$contentTypeElementType->contentType->name];
                    if($content["content"] == $value) {
                        array_push($elements,$element); 
                    }
                } else if($value == 0) {
                    array_push($elements,$element);     
                }
            }
        }
        return $elements;
    }

    public static function countAllTodayByContentAndContentTypeIdElemntTypeIdAndDate($id_content_type_element_type,$date) 
    {
        $count_elements = [];
        $count_elements[0] = 0;
        $count_elements[1] = 0;
        $contentTypeElementType = ContentTypeElementType::find($id_content_type_element_type);
        foreach (self::createdAt($date)->cursor() as $key => $element) {
            $contents =  Content::actives()->elementId($element->id_element)->get();
            $element->contents = self::contentsFormatt($contents);
            if($contentTypeElementType != null) {
                if(isset($element->contents[$contentTypeElementType->contentType->name])) {
                    $content = $element->contents[$contentTypeElementType->contentType->name];
                    if($content["content"] == 0) {
                        $count_elements[0]++;
                    } else {
                        $count_elements[1]++;
                    }
                } else {
                    $count_elements[0]++;
                }
            }
        }
        return $count_elements;
    }

    private static function contentsFormatt($contents) 
    {
        $contentsFormatt = []; 
        foreach ($contents as $key => $content) {
            $contentJson = json_decode($content->content);
            
            $contentsFormatt[$content->contentTypeElementType->contentType->name] = array(
                "content" => ( $content->contentTypeElementType->contentType->type == "json") ? $contentJson->{$content->contentTypeElementType->contentType->name} : $content->content,
                "type" => $content->contentTypeElementType->contentType->type,
                "name" => $content->contentTypeElementType->contentType->name
            );
        }
        return $contentsFormatt;
    }

    public static function getRandomElementByElementTypeId($id_element_type) 
    {
        return self::actives()->elementTypeId($id_element_type)
            ->inRandomOrder()
            ->first();  
    }

    public function delete() 
    {
        foreach (Content::actives()->elementId($this->id_element)->cursor() as $key => $content) {
            if($content->contentTypeElementType->contentType->name == strtolower($content->contentTypeElementType->elementType->name)) {
                \Storage::delete($content->content);
                $content->delete();
            } else {
                $content->delete();
            }
        }
        return parent::delete();
    }


}
