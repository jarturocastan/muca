<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id_rol';
    
    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactives($query)
    {
        return $query->where('status', 0);
    }
    
    public function users(){
        return $this->hasMany('App\Models\User','id_user');
    }
}
