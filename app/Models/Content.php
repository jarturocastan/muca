<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Element;
use App\Models\ContentTypeElementType;

class Content extends Model
{
    protected $table = 'contents';
    protected $primaryKey = 'id_content';
 
    public function element()
    {
        return $this->belongsTo('App\Models\Element','id_element');
    }

    public function contentTypeElementType()
    {
        return $this->belongsTo('App\Models\ContentTypeElementType','id_content_type_element_type');
    }

    public function contentStructureParent()
    {
        return $this->hasOne('App\Models\ElementStructure', 'id_content_parent');
    }

    public function contentStructureChild()
    {
        return $this->hasOne('App\Models\ElementStructure', 'id_content_child');
    }

    /**Scopes */
    public function scopeElementId($query,$id_element) 
    {
        return $query->where('id_element',$id_element);
    }

    public function scopeElementTypeContentTypeId($query,$id_content_type_element_type) 
    {
        return $query->where('id_content_type_element_type',$id_content_type_element_type);
    }

    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    /**Functions */
    public static function getFindAllFavoriteContentByContentTypeIdAndElementTypeId($id_element_type,$id_content_type,$value)  
    {
        $contentTypeElementType = ContentTypeElementType::findByElmentTypeIdAndContentTypeId($id_element_type,$id_content_type);
        return self::elementTypeContentTypeId($contentTypeElementType->id_content_type_element_type)
            ->with(['element'=>function($query){
                $query->whereDate('created_ar',date('Y-m-d'));
            }])
            ->where("content",'{"'.$contentTypeElementType->contentType->name.'": '.$value.'}')
            ->get();  
    }

    public static function getRandomFavoriteContentByContentTypeIdAndElementTypeId($id_element_type,$id_content_type,$value)  
    {
        $contentTypeElementType = ContentTypeElementType::findByElmentTypeIdAndContentTypeId($id_element_type,$id_content_type);
        return self::actives()
            ->elementTypeContentTypeId($contentTypeElementType->id_content_type_element_type)
            ->where("content",'{"'.$contentTypeElementType->contentType->name.'": '.$value.'}')
            ->inRandomOrder()
            ->first();  
    }

    public static function getPathContentByElementIdAndContentTypeElementType($id_element,$id_content_type_element_type) 
    {
        return self::actives()
            ->elementTypeContentTypeId($id_content_type_element_type)
            ->elementId($id_element)
            ->first();
    }

    public function delete() 
    {
        $file = $this->id_element.'/'.$this->id_content;
        \File::deleteDirectory(storage_path('app/public').$file);
        return parent::delete();
    }
}
