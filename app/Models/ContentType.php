<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    protected $table = 'content_types';
    protected $primaryKey = 'id_content_type';

    public function contentTypesElementTypes()
    {
        return $this->hasMany('App\Models\ContentTypeElementType','id_content_type');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Content','id_content');
    }

    public function contentTypeValidators()
    {
        return $this->hasMany('App\Models\ContentTypeValidator','id_content_type_validator');
    }
}
