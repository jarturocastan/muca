<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Validator extends Model
{
    protected $table = 'validator';
    protected $primaryKey = 'id_validator';

    public function contentTypeValidators()
    {
        return $this->hasMany('App\Models\ContentTypeValidator','id_content_type_validator');
    }
}
