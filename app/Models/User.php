<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $primaryKey = 'id_user';

    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function AauthAcessToken(){
        return $this->hasMany('App\Models\OauthAccessToken');
    }

    public function rol()
    {
        return $this->belongsTo('App\Models\Rol','id_rol');
    }

    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactives($query)
    {
        return $query->where('status', 0);
    }

    /**Function */
    public static function findAllActives() 
    {
        return self::actives()->get();
    }

    public static function findAllInactives() 
    {
        return self::inactives()->get();
    }

}
