<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElementType extends Model
{
    protected $table = 'element_types';
    protected $primaryKey = 'id_element_type';

    public function contentTypesElementTypes()
    {
        return $this->hasMany('App\Models\ContentTypeElementType','id_element_type');
    }

    public function elements()
    {
        return $this->hasMany('App\Models\Element','id_element');
    }

    public function scopeInactives($query)
    {
        return $query->where('status', 0);
    }

    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    /**Function */

    public static function findAllActives() 
    {
        return self::actives()->get();
    }

    public static function findAllInactives() 
    {
        return self::inactives()->get();
    }
}
