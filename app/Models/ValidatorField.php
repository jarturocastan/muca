<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValidatorField extends Model
{
    protected $table = 'validator_field';
    protected $primaryKey = 'id_validator_field';

    public function contentTypeValidator()
    {
        return $this->belongsTo('App\Models\ContentTypeValidator','id_content_type_validator');
    }
}
