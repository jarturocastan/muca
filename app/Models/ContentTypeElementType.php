<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentTypeElementType extends Model
{
    protected $table = 'content_types_element_types';
    protected $primaryKey = 'id_content_type_element_type';

    public function elementType()
    {
        return $this->belongsTo('App\Models\ElementType','id_element_type');
    }

    public function contentType()
    {
        return $this->belongsTo('App\Models\ContentType','id_content_type');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Content','id_content');
    }
    /**Scopes */
    public function scopeElementTypeId($query,$id_element_type) 
    {
        return $query->where('id_element_type',$id_element_type);
    }

    public function scopeContentTypeId($query,$id_content_type) 
    {
        return $query->where('id_content_type',$id_content_type);
    }

    public function scopeActives($query)
    {
        return $query->where('status', 1);
    }

    /**Functions */
    public static function findByElmentTypeIdAndContentTypeId($id_element_type,$id_content_type) 
    {
        return self::actives()
            ->elementTypeId($id_element_type)
            ->contentTypeId($id_content_type)
            ->first();
    }
}
