<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Muca</title>
  <base href="./">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="logo.png">
<link rel="stylesheet" href="{!! asset('dist/styles.400b3a2d406743820f9a.css')!!}"></head>
<body>
  <app-root></app-root>
  <script>
    $(document).foundation();
  </script>
  <script type="text/javascript" src="{!! asset('dist/runtime.a66f828dca56eeb90e02.js')!!}">
  </script><script type="text/javascript" src="{!! asset('dist/polyfills.7a0e6866a34e280f48e7.js')!!}">
  </script><script type="text/javascript" src="{!! asset('dist/scripts.5c1a5ce19569984d80b1.js')!!}">
  </script><script type="text/javascript" src="{!! asset('dist/main.e60fc595c43ffd119ecf.js')!!}"></script>
</body>
</html>
