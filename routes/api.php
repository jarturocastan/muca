<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('logout','Auth\LoginController@logoutApi');

/**Urls de la app */
//Route::group(['middleware' => 'authAppUser'], function () {
    Route::group(['prefix' => 'app-user'], function () {
        Route::group(['prefix' => 'element'], function () {
            Route::get('getAudio/{id_element_type}','Api\ElementController@element');
            Route::post('saveAudio/{id_element_type}','Api\ElementController@storage');
            Route::get('file/{id_element}','Api\ElementController@file');                        
        });
    });
//});
Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'element'], function () {
        Route::get('getZip/','Api\ElementController@getZip');              
    });
});
/**Urls de la web */
Route::group(['middleware' => 'authAdministratorUser'], function () {  
    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'element'], function () {
            Route::get('getAudio/{id_element_type}','Api\ElementController@element');
            Route::post('saveAudio/{id_element_type}','Api\ElementController@storage');
            Route::get('findAllActives','Api\ElementController@findAllActives');
            Route::get('findAllTodayActives','Api\ElementController@findAllTodayActives');
            Route::get('findAllToday','Api\ElementController@findAllToday');
            Route::get('findAllFavoriteToday','Api\ElementController@findAllFavoriteToday');
            Route::get('findAllAttentionToday','Api\ElementController@findAllAttentionToday');
            Route::get('addFavorite/{id_element}','Api\ElementController@addFavorite');
            Route::get('file/{id_element}','Api\ElementController@file');            
            Route::get('addAttention/{id_element}/{attention}','Api\ElementController@addAttention');            
            Route::get('delete/{id_element}','Api\ElementController@delete');            
            Route::get('modifyStatus/{id_element}','Api\ElementController@modifyStatus');          
            Route::post('findAllByDate/','Api\ElementController@findAllByDate');             
            Route::post('getCalendarInfo/','Api\ElementController@getCalendarInfo');             
            Route::post('deleteMany/','Api\ElementController@deleteMany'); 
            Route::post('createZip/','Api\ElementController@creteZip');  
            Route::post('findAllFavoriteByDate/','Api\ElementController@findAllFavoriteByDate');  
            Route::post('findAllAttentionByDate/','Api\ElementController@findAllAttentionByDate');  
        });

        Route::group(['prefix' => 'element-type'], function() {
            Route::post('save','Api\ElementTypeController@storage');
            Route::get('findAllActives','Api\ElementTypeController@findAllActives');
            Route::get('findAllInactives','Api\ElementTypeController@findAllInactives');
            Route::get('getBlockWords/{id_element_type}','Api\ElementTypeController@getBlockWords');
            Route::post('saveBlockWord/{id_element_type}','Api\ElementTypeController@saveBlockWord');
            Route::group(['prefix' => 'content-type'], function () {
                Route::post('save/{id_element_type}','Api\ContentTypeController@storage');
            });
        });

        Route::group(['prefix' => 'user'], function() {
            Route::get('findAllActives/','Api\UserController@findAllActives');
            Route::get('findAllInactives/','Api\UserController@findAllInactives');
            Route::get('modifyStatus/{id_user}','Api\UserController@modifyStatus');
            Route::get('delete/{id_user}','Api\UserController@delete');   
            Route::post('deleteMany/','Api\UserController@deleteMany');   
            Route::post('save/','Api\UserController@storage');   
            
            Route::group(['prefix' => 'rol'], function() {
                Route::get('findAllActives/','Api\RolController@findAllActives');
                
            });  
        });
        
    });
});

